# Categorize spam email using a Naive Bayes Classifier
# By Andrew Cvitanovich

# Using the Spam Assassin Public Corpus: https://spamassassin.apache.org/old/publiccorpus/

# Description:
# 
# Preprocessing emails: Each email file is opened, the email body is isolated, 
# irrelevant html tags and anything else that's probably not a word is remove.
# Then bigrams are formed from the words in the email body and this is saved as a CSV file.
#
# Naive Bayes Classifier:
# 200 training emails are saved in the "train" folder to train the classifier (50% Spam 50% Ham)
# The remaining emails are put in a "test" folder
# Test Email Stats: 401 spam, 2451 ham, 2852 total, 14.1% spam, 95.9% ham

import email
import sys
import re
import csv
import os

# find bigrams that have a difference in probability of at least this amount between ham and spam
probability_difference = 0.05

def remove_url(h):
    pattern = re.compile(r'https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+')
    txt = re.sub(pattern,'',h)
    return txt

def remove_html(h):
    pattern = re.compile(r'<.*?>')
    txt = re.sub(pattern,'',h)
    return txt

def alphabet_only(s):
    regex = re.compile(r'[^a-zA-Z]')
    return regex.sub('',s)

def process_bigrams(file, directory, output_file, output_directory, all_bigrams, fileno):
    input = directory + "/" + file
    output = output_directory + "/" + output_file + ".csv"
    
    #print "Processing bigrams for " + input
    # load email file
    with open(input, 'r') as f:
        data = f.read()
    
    # get email message using email module
    msg = email.message_from_string(data)
    
    #process multipart emails and just get the body of email
    body=""
    if msg.is_multipart():
        for part in msg.walk():
            content_type = part.get_content_type()
            if content_type == 'text/plain' or content_type == 'text/html':
                body += part.get_payload(decode=True)
    else:
        body = msg.get_payload(decode=True)
    
    # remove urls, html and newlines
    # split by spaces into words
    body = body.replace('\n','')
    body = remove_url(body)
    body = remove_html(body)
    body = body.split(" ")
    
    # create bigrams as a list of 1x2 sublists
    # N = 2 for bigrams
    N = 2
    
    pairs = [body[i:i+N] for i in range(len(body)-N+1)]
    
    bigrams = []
    for p in pairs:
        if p[0] != '' and p[1] != '':
            bigrams.append( (p[0], p[1]) )

    bigrams_fixed = []
    for b in bigrams:
        first = alphabet_only(b[0])
        second = alphabet_only(b[1])
        if first == '' or second == '':
            continue
        bigrams_fixed.append( (first, second) )
        all_bigrams.append( (first, second, fileno) )
    
    with open(output,'wb') as resultFile:
        o = csv.writer(resultFile)
        for b in bigrams_fixed:
            o.writerow(list(b))
    
    #print "Saved bigrams as " + output
        
spam_path = "/home/andrew/cis471/spam/"
ham_path = "/home/andrew/cis471/easy_ham/"
train_path = "/home/andrew/cis471/train/"
test_path = "/home/andrew/cis471/test/"
NTrain = 100 # number to train for each subset (ham vs. spam). Total train = 2*NTrain

# open each spam email, process bigrams and save as csv file
# first NTrain files get saved in train_path
# remaining files get saved in test_path

count = 0
all_train_spam_bigrams = []
all_test_spam_bigrams = []
for fn in os.listdir(spam_path):
    if count < NTrain:
        outdir = train_path
        # process file for bigrams and save
        outfn = fn + ".spam"
        process_bigrams(fn, spam_path, outfn, outdir, all_train_spam_bigrams, count)
    else:
        outdir = test_path
        # process file for bigrams and save
        outfn = fn + ".spam"
        process_bigrams(fn, spam_path, outfn, outdir, all_test_spam_bigrams, count)
    count += 1;


# sort all_train_spam_bigrams (fileno, first word, second word)
all_train_spam_bigrams.sort(key=lambda tup: tup[2], reverse = True)
all_train_spam_bigrams.sort(key=lambda tup: tup[0])
all_train_spam_bigrams.sort(key=lambda tup: tup[1])

# Likelihood: frequencies of bigrams (Probability that emails have this bigram)
freq_train_spam_bigrams = []
current = all_train_spam_bigrams[0]
count = 1
for i in range(1,len(all_train_spam_bigrams)):
    # bigram match
    if current[:2] == all_train_spam_bigrams[i][:2]:
        # check if this bigram is from a new file
        if current[2] != all_train_spam_bigrams[i][2]:
            # new current bigram signature
            current = all_train_spam_bigrams[i]
            # different files, update count
            count += 1
    else:
        # add entry for this bigram to frequency list
        bg_freq = (current[0], current[1], float(count)/NTrain)
        freq_train_spam_bigrams.append(bg_freq)
        # new current bigram signature
        current = all_train_spam_bigrams[i]
        # begin new count
        count = 1

# sort by frequency
#freq_train_spam_bigrams.sort(key=lambda tup: tup[2])

if(0):
    # save to csv (frequencies of bigrams for spam)
    output = train_path + "freq_train_spam_bigrams.csv"
    with open(output,'wb') as resultFile:
        o = csv.writer(resultFile)
        for b in freq_train_spam_bigrams:
            o.writerow(list(b))

count = 0
all_train_ham_bigrams = []
all_test_ham_bigrams = []
for fn in os.listdir(ham_path):
    if count < NTrain:
        outdir = train_path
        # process file for bigrams and save
        outfn = fn + ".ham"
        process_bigrams(fn, ham_path, outfn, outdir, all_train_ham_bigrams, count)
    else:
        outdir = test_path
        # process file for bigrams and save
        outfn = fn + ".ham"
        process_bigrams(fn, ham_path, outfn, outdir, all_test_ham_bigrams, count)
    count += 1;

# sort all_train_ham_bigrams (fileno, first word, second word)
all_train_ham_bigrams.sort(key=lambda tup: tup[2], reverse = True)
all_train_ham_bigrams.sort(key=lambda tup: tup[0])
all_train_ham_bigrams.sort(key=lambda tup: tup[1])


# Likelihood: frequencies of bigrams (Probability that emails have this bigram)
freq_train_ham_bigrams = []
current = all_train_ham_bigrams[0]
count = 1
for i in range(1,len(all_train_ham_bigrams)):
    # bigram match
    if current[:2] == all_train_ham_bigrams[i][:2]:
        # check if this bigram is from a new file
        if current[2] != all_train_ham_bigrams[i][2]:
            # new current bigram signature
            current = all_train_ham_bigrams[i]
            # different files, update count
            count += 1
    else:
        # add entry for this bigram to frequency list
        bg_freq = (current[0], current[1], float(count)/NTrain)
        freq_train_ham_bigrams.append(bg_freq)
        # new current bigram signature
        current = all_train_ham_bigrams[i]
        # begin new count
        count = 1

# sort by frequency
#freq_train_ham_bigrams.sort(key=lambda tup: tup[2])

# save to csv (frequencies of bigrams for ham)
if(0):
    output = train_path + "freq_train_ham_bigrams.csv"
    with open(output,'wb') as resultFile:
        o = csv.writer(resultFile)
        for b in freq_train_ham_bigrams:
            o.writerow(list(b))

# combine frequencies for bigrams found in ham and spam (training)
train_freq = []
print "Combining..."
for sbg in freq_train_spam_bigrams:
    found_match = 0
    cnt = 0
    for hbg in freq_train_ham_bigrams:
        cnt += 1
        if sbg[:2] == hbg[:2]:
            diff = sbg[2] - hbg[2]
            if abs(diff) > 0.05:
                # found matching bigram, save these frequencies if they differ by 5%
                # (first word, second word, spam frequency, ham frequency)
                print "match:"
                print hbg
                print sbg
                train_freq.append( (sbg[0], sbg[1], sbg[2], hbg[2], diff) )
            found_match = 1
            break
        else:
            if cnt == NTrain and hbg[2] > 0.05:
                train_freq.append( (hbg[0], hbg[1], 0, hbg[2], hbg[2]) )
    if(not found_match):
        if sbg[2] > 0.05:
            train_freq.append( (sbg[0], sbg[1], sbg[2], 0, sbg[2]) )
            

# sort combined probabilities by difference
train_freq.sort(key=lambda tup: tup[4], reverse=True)

# save to csv (probabilities for spam/ham of bigrams found in both spam and ham)
output = train_path + "freq_train_both_bigrams.csv"
with open(output,'wb') as resultFile:
    o = csv.writer(resultFile)
    for b in train_freq:
        o.writerow(list(b))

