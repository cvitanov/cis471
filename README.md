# cis471

# spam filter
spam_naive_bayes_train.py - script to train Naive Bayes model  
spam_naive_bayes_test.py - test the model  
spam/ - Spam Assasin corpus of spam  
easy_ham/ - non-spam  
train/ - training data (bigrams)  
test/ - testhing data  

# finding company names
company_names.py - script to extract company names using regex  
reuter_corpus/ - Reuters News corpus  
regex_output.txt - Sample output of the regex matching of company names  

