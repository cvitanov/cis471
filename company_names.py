import re
from random import *
from lxml.etree import fromstring
directory = "reuters_corpus"
file = "reut2-000.sgm"
fpath = directory + "/" + file
alpha = 0.1 # learning rate

def extract_sentences(file):
    file = re.sub("\n"," ",file)
    pattern = r'<BODY>([^<]+)</BODY>'
    articles = re.findall(pattern,file)
    #pattern = r'<TITLE>([^<]+)</TITLE>'
    #titles = re.findall(pattern,file)
    #pattern = r"(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s"
    articles_processed = []
    article_keys = []
    count = 0
    for a in articles:
        s = re.findall("[A-Z].*?[\.!?]", a, re.MULTILINE | re.DOTALL )
        for i in range(0,len(s)):
            s[i] = re.sub("\.","",s[i])
            s[i] = s[i].split(" ")
        articles_processed.append(s)
        article_keys.append(count)
        count += 1
    article_dict = dict(zip(article_keys,articles_processed))
    return article_dict

# all ones
def ones(sz):
    out = []
    for i in range(0,sz):
        out.append(1)
    return out

# all zeros
def zeros(sz):
    out = []
    for i in range(0,sz):
        out.append(0)
    return out

# uniformly random list
def uniform_randlist(low, high, sz):
    out = []
    for i in range(0,sz):
        out.append(uniform(low,high))
    return out

def rand_binary(l):
    r = uniform_randlist(0, 1, len(l))
    for elem in r:
        if r > 0.5:
            r = 1
        else:
            r = 0
    return r 
    
# score a labeling of a bigram using feature functions F, weights W, and bigram b and labels L
def score_labeling(F,W,b,L):
    score = 0
    # loop through feature functions
    for j in range(0,len(F)):
        # first word
        # calculate score for each feature function of labelings of each word
        f = F[j]
        score += W[j] * f(L,b,0) # feature function score for position i with labels L and sentence S
        # second word
        # calculate score for each feature function of labelings of each word
        f = F[j]
        score += W[j] * f(L,b,1)
    return float(score)
  
# calculation conditional probability of a labeling given a bigram using feature functions, and weights
# note: only works with labeling lengths of 2 (bigrams)
def prob_label(F, W, b, L):
    # Euler's number
    e = 2.7182818284590452353602874713527
    # numerator exp(score)
    score = score_labeling(F,W,b,L)
    top = e ** score
    # denominator (the normalization factor, try all possible labelings and sum probabilites)
    normp = 0.01
    normp += score_labeling(F,W,b,[0,1])
    normp += score_labeling(F,W,b,[1,0])
    normp += score_labeling(F,W,b,[0,0])
    normp += score_labeling(F,W,b,[1,1])
    #print "normp = " + str(normp)
    # return normalized conditional probabilitiy of labeling
    return float(top) / normp

# gradient of log prob training example (With respect to weight w_i)
# calculate for one bigram at a time
def grad_wt(f,wt,b,L):
    # sum feature values for all words in sentence
    #print wt
    sumf = f(L,b,0)
    sumf += f(L,b,1)
    # expected contribution (all possible labelings)
    
    pl_prime = prob_label([f],[wt],b,[0,1]) * (f([0,1],b,0) + f([0,1],b,1))
    pl_prime += prob_label([f],[wt],b,[1,0]) * (f([1,0],b,0) + f([1,0],b,1))
    pl_prime += prob_label([f],[wt],b,[0,0]) * (f([0,0],b,0) + f([0,0],b,1))
    pl_prime += prob_label([f],[wt],b,[1,1]) * (f([1,1],b,0) + f([1,1],b,1))
    return sumf - pl_prime

# alpha is learning rate
def update_wt(f,wt,S,L,alpha):
    gradient = 0
    for i in range(0,len(S)):
        if (i + 1) == len(S):
            break
        gradient += grad_wt(f,wt,S[i:i+2],L[i:i+2])
    return wt + alpha*gradient

# load email file
with open(fpath, 'r') as f:
    data = f.read()


f.close()


# extract words for each sentence and each article
data = extract_sentences(data)

# feature functions (x = hidden layer (labelings), e = words in sentence, i = index of current word)
# f1: e_i is capitalized and this word is labeled 1 for company
f1 = lambda x,e,i : 1 if (x[i] == 1 and re.match("[A-Z]",e[i]) != None) else 0
# f2: e_i matches a words starting with "Co" and this word is 1 for company
f2 = lambda x,e,i : 1 if (x[i] == 1 and re.match("Co*",e[i]) != None) else 0
# f3: e_i matches a words starting with "Inc" (and label 1 for company)
f3 = lambda x,e,i : 1 if (x[i] == 1 and re.match("Inc*",e[i]) != None) else 0
# f4: e_i matches any of several common words in company names
f4 = lambda x,e,i : 1 if (x[i] == 1 and re.match("(Group|Lines|Equity|Holdings|Airline|International|Financial|Technologies)",e[i]) != None) else 0
# f5: e_i matches any of several common words in company names (More common words)
f5 = lambda x,e,i : 1 if (x[i] == 1 and re.match("(Insurance|Industries|Services|Supply|Supplies|Stores|Foods|Energy|Bank|Products|Savings and Loan)",e[i]) != None) else 0
# f6: e_i matches the word "Ltd" or "Ltd."
f6 = lambda x,e,i : 1 if (x[i] == 1 and re.match("Ltd(\.)?",e[i]) != None) else 0
# f7: previous word is company (1 for company) and this word is caps
f7 = lambda x,e,i : 1 if ((i > 0) and (x[i-1] == 1) and re.match("[A-Z]",e[i])) else 0


# Feature functions
F = [f1,f2,f3,f4,f5,f6,f7]

# Random Weights to begin#W = uniform_randlist(-1, 1, len(F))


# test regex on all articles
for d in data:
    for sentence in data[d]:
        count = 0
        #print sentence
        last_word = ""
        for word in sentence:
             
            count += 1
            if re.match("[A-Z]",word) != None:
                if re.match("Co*",word) != None:
                    print "Found company word # " + str(count) + " word = " + word
                if re.match("Inc*",word) != None:
                    print "Found company word # " + str(count) + " word = " + word
                if re.match("(Group|Lines|Equity|Holdings|Airline|International|Financial|Technologies)",word) != None:
                    print "Found company word # " + str(count) + " word = " + word
                if re.match("(Insurance|Industries|Services|Supply|Supplies|Stores|Foods|Energy|Bank|Products|Savings and Loan)",word) != None:
                    print "Found company word # " + str(count) + " word = " + word
            elif count > 0 and re.match("[A-Z]",last_word):
                if re.match("Co*",word) != None:
                    print "Found company word # " + str(count) + " word = " + word
                if re.match("Inc*",word) != None:
                    print "Found company word # " + str(count) + " word = " + word
                if re.match("(Group|Lines|Equity|Holdings|Airline|International|Financial|Technologies)",word) != None:
                    print "Found company word # " + str(count) + " word = " + word
                if re.match("(Insurance|Industries|Services|Supply|Supplies|Stores|Foods|Energy|Bank|Products|Savings and Loan)",word) != None:
                    print "Found company word # " + str(count) + " word = " + word
             
            last_word = word


