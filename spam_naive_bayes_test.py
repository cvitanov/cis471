# Testing Naive Bayes Classifier (bigrams)
# Andrew Cvitanovich

import csv
import os
import email
import re
import random

# Bernoulli Naive Bayes Model using bigrams from 100 training emails.
# Bernoulli Model assumes data distributed according to multivariate Bernoulli distributions
# Results:
# 2858 emails tested
# Miss (False Neg, Type II Error) 74 Hit (True Positive) 327 CR (True Negative) 1912 FA (False Pos, Type I Error) 539
#  hit rate (recall, sensitivity, or true positive rate) = 0.815461346633 fa rate (false positive rate, or fall-out) = 0.219910240718 
#  accuracy = (TP + TN) / TOTAL = 78.5%
#  precision = 37.8% (Fraction marked spam that's actually spam)
#  recall/sensitivity = 81.5% (Fraction of spam marked spam)
#  F1 = 2TP / (2TP + FP + FN) = 2*327 / (2*327 + 539 + 74) = 2*Precision*Recall/(Precision + Recall) = 0.516
filename = "/home/andrew/cis471/train/freq_train_both_bigrams.csv"
trained_bigrams = [tuple(row) for row in csv.reader(open(filename, 'rU'))]
test_path = "/home/andrew/cis471/test/"
trained_bigrams.sort(key=lambda tup: tup[3], reverse=False)

def remove_url(h):
    pattern = re.compile(r'https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+')
    txt = re.sub(pattern,'',h)
    return txt

def remove_html(h):
    pattern = re.compile(r'<.*?>')
    txt = re.sub(pattern,'',h)
    return txt

def alphabet_only(s):
    regex = re.compile(r'[^a-zA-Z]')
    return regex.sub('',s)

# P(x_i|y) = P(i|y)x_i + (1-P(i|y))(1-x_i)
def isSpamBernoulli(theBigrams,trainbg):
    jp_spam = 1
    jp_ham = 1
    for tb in trainbg:
        found_match = 0
        for b in theBigrams:
            if tb[:2] == b[:2]:
                found_match = 1
                pspam = float(tb[2])
                pham = float(tb[3])
                jp_spam *= pspam
                jp_ham *= pham
                break
        if(not found_match):
            pspam = float(tb[2])
            pham = float(tb[3])
            jp_spam *= 1 - pspam
            jp_ham *= 1 - pham 
    
    norm_jp_spam = 0.14*jp_spam
    norm_jp_ham = 0.96*jp_ham
    if norm_jp_spam > norm_jp_ham:
        return True
    return False
            
# Simple Bayes Classifier without Bernoulli Model
def isSpamSimple(theBigrams,trainbg):
    jp_spam = 1
    jp_ham = 1
    for b in theBigrams:
        for tb in trainbg:
            if tb[:2] == b[:2]:
                pspam = float(tb[2])
                if pspam == 0:
                    pspam = 0#0.001
                pham = float(tb[3])
                if pham == 0:
                    pham = 0#0.001
                jp_spam *= pspam
                jp_ham *= pham
                break
    
    norm_jp_spam = 0.14*jp_spam
    norm_jp_ham = 0.96*jp_ham
    if norm_jp_spam > norm_jp_ham:
        return True
    return False

# test classifier on test set:
misses = 0
hits = 0
cr = 0
fa = 0
count = 0
files = os.listdir(test_path)
#files = random.sample(files,1000)
for fn in files:
    count += 1
    print count

    l = len(fn)
    tl = fn[l-8:l] # file ending
    # get email type (spam or ham)
    if tl == ".ham.csv" or tl == "spam.csv":
        C = tl[:4]
        if tl[0] == ".":
            C = C[1:]
        
        # test classifier
        fname = test_path + fn
        bgrams = [tuple(row) for row in csv.reader(open(fname, 'rU'))]
        if(isSpamBernoulli(bgrams,trained_bigrams)):
            if C == "spam":
                hits += 1
            else:
                fa += 1
        else:
            if C == "spam":
                misses += 1
            else:
                cr += 1
                
print "Miss {} Hit {} CR {} FA {}".format(misses, hits, cr, fa)
print "Accuracy: hit rate = {} fa rate = {} ".format(float(hits)/(hits+misses), float(fa)/(fa + cr))
